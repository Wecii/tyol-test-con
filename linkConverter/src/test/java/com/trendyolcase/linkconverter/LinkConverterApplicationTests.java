package com.trendyolcase.linkconverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.trendyolcase.linkconverter.Controller.ConverterController;
import com.trendyolcase.linkconverter.Entity.Traffic;
import com.trendyolcase.linkconverter.Object.ResponseJson;
import com.trendyolcase.linkconverter.Object.ServiceInput;
import com.trendyolcase.linkconverter.Repository.TrafficRepository;
import com.trendyolcase.linkconverter.Service.ConverterService;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.SortedSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations= "classpath:application-test.properties")
public class LinkConverterApplicationTests {

    @Autowired
    private ConverterController converterController;

    @Autowired
    private TrafficRepository trafficRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
        assertThat(converterController).isNotNull();
    }

    @Test
    @Transactional
    public void mysqlExceptionTesting(){
        Traffic testTraffic = new Traffic();
        testTraffic.setCreatedAt(null);
        testTraffic.setFromRaw("test");
        testTraffic.setFromType("web");
        testTraffic.setTargetRaw("test");
        trafficRepository.save(testTraffic);

        List<Traffic> traffics = trafficRepository.findByFromType("web");
        assertNull(traffics.get(0).getCreatedAt());

        System.out.println("AssertNull Test Completed");

    }
    @Test
    @Transactional
    public void mysqlTestingWithTestCases() throws IOException {

        File resource = new ClassPathResource("data/test-cases.txt").getFile();
        String testCases = new String(Files.readAllBytes(resource.toPath()));
        String[] lines = testCases.split("\n");

        for(String line : lines) {
            String[] parts = line.split("###");
            String type = parts[0].trim();
            String input = parts[1].trim();
            String output = parts[2].trim();

            Traffic traffic = new Traffic(input,output,type);
            trafficRepository.save(traffic);

        }

        List<Traffic> webToDeeplinkConversions = trafficRepository.findByFromType("web");
        for(Traffic t : webToDeeplinkConversions){
            System.out.println(t.toString());
            assertNotNull(t.getId());
            assertNotNull(t.getFromRaw());
            assertNotNull(t.getTargetRaw());
            assertNotNull(t.getFromType());
            assertNotNull(t.getCreatedAt());
            System.out.println("Success");
        }

        System.out.println("AssertNotNull Test Completed");

    }
    @Test
    @Transactional
    public void unitTestingWithTestCases() throws Exception {

        File resource = new ClassPathResource("data/test-cases.txt").getFile();
        String testCases = new String(Files.readAllBytes(resource.toPath()));
        String[] lines = testCases.split("\n");
        for(String line : lines){

            String[] parts = line.split("###");
            String type = parts[0].trim();
            String input = parts[1].trim();
            String output = parts[2].trim();

            ServiceInput serviceInputRequest = new ServiceInput(input);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            String requestJson=ow.writeValueAsString(serviceInputRequest);

            MvcResult result = this.mockMvc
                    .perform(
                            post("/convert/"+type).contentType(APPLICATION_JSON_UTF8).content(requestJson)
                    )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andReturn();

            String json = result.getResponse().getContentAsString();
            ResponseJson responseJson = new ObjectMapper().readValue(json, ResponseJson.class);
            assertEquals(output,responseJson.getData());

        }

        System.out.println("Unit Test Completed");

    }

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);

}
