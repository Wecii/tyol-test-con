package com.trendyolcase.linkconverter.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InputNotValidException extends RuntimeException{
    public InputNotValidException(String exception) {
        super(exception);
    }
}
