package com.trendyolcase.linkconverter.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InputTypeMatchingException extends RuntimeException{
    public InputTypeMatchingException(String exception) {
        super(exception);
    }
}