package com.trendyolcase.linkconverter.Repository;

import com.trendyolcase.linkconverter.Entity.Traffic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrafficRepository extends JpaRepository<Traffic, Long> {
    List<Traffic> findByFromType(String type);
    Traffic findByFromRaw(String addr);
    Traffic findByTargetRaw(String addr);
}
