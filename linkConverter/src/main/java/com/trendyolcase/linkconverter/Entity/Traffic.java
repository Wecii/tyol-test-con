package com.trendyolcase.linkconverter.Entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Traffic implements Serializable {

    private static final long serialVersionUID = 1234567L;

    public Traffic() {
        this.createdAt = new Date();
    }

    public Traffic(String fromRaw, String targetRaw, String fromType) {
        this.fromRaw = fromRaw;
        this.targetRaw = targetRaw;
        this.fromType = fromType;
        this.createdAt = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="from_raw", nullable = false)
    private String fromRaw;

    @Column(name="target_raw", nullable = false)
    private String targetRaw;

    @Column(name="from_type", nullable = false)
    private String fromType;

    @Column(name="created_at", updatable = false)
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromRaw() {
        return fromRaw;
    }

    public void setFromRaw(String fromRaw) {
        this.fromRaw = fromRaw;
    }

    public String getTargetRaw() {
        return targetRaw;
    }

    public void setTargetRaw(String targetRaw) {
        this.targetRaw = targetRaw;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Traffic{" +
                "id=" + id +
                ", fromRaw='" + fromRaw + '\'' +
                ", targetRaw='" + targetRaw + '\'' +
                ", fromType='" + fromType + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
