package com.trendyolcase.linkconverter.Constant;

public abstract class Constants {

    public static final String deeplinkDefaultScheme = "ty://?";

    public static final String ampersand = "&";
    public static final String equal = "=";
    public static final String slash = "/";

    public static final String webURLProductBrace = "-p-";
    public static final String webURLSearchQueryDefaultText = "sr?q=";

    public static final String utf8Encoding = "UTF-8";

    public static final String webURLSourceType = "web";
    public static final String deeplinkSourceType = "deeplink";

    public static final String inputNotValidExceptionDefaultText = "Web URL or Deeplink is not valid.";
    public static final String inputTypeMatchingExceptionDefaultText = "The submitted input and endpoint must be compatible.";
    public static final String inputParsingExceptionDefaultText = "The submitted input could not be parsed.";
    public static final String defaultExceptionText = "An exception occurred.";

}
