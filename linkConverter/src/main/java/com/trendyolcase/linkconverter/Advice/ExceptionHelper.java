package com.trendyolcase.linkconverter.Advice;

import com.trendyolcase.linkconverter.Constant.Constants;
import com.trendyolcase.linkconverter.Controller.ConverterController;
import com.trendyolcase.linkconverter.Exception.InputNotValidException;
import com.trendyolcase.linkconverter.Exception.InputParsingException;
import com.trendyolcase.linkconverter.Exception.InputTypeMatchingException;
import com.trendyolcase.linkconverter.Object.ResponseJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(basePackageClasses = ConverterController.class)
public class ExceptionHelper extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHelper.class);

    @ExceptionHandler(InputNotValidException.class)
    public final ResponseEntity<ResponseJson> handleInputNotValidException(InputNotValidException ex, WebRequest request) {
        logger.error(ex.getMessage(),ex);
        ResponseJson responseJson = new ResponseJson(0,ex.getLocalizedMessage(), Constants.inputNotValidExceptionDefaultText);
        return new ResponseEntity<>(responseJson,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InputTypeMatchingException.class)
    public final ResponseEntity<ResponseJson> handleInputTypeMatchingException(InputTypeMatchingException ex, WebRequest request){
        logger.error(ex.getMessage(),ex);
        ResponseJson responseJson = new ResponseJson(0,ex.getLocalizedMessage(),Constants.inputTypeMatchingExceptionDefaultText);
        return new ResponseEntity<>(responseJson,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InputParsingException.class)
    public final ResponseEntity<ResponseJson> handleInputParsingException(InputParsingException ex, WebRequest request){
        logger.error(ex.getMessage(),ex);
        ResponseJson responseJson = new ResponseJson(0,ex.getLocalizedMessage(),Constants.inputParsingExceptionDefaultText);
        return new ResponseEntity<>(responseJson,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        logger.error(ex.getMessage(),ex);
        ResponseJson responseJson = new ResponseJson(0,ex.getLocalizedMessage(),Constants.defaultExceptionText);
        return new ResponseEntity<Object>(responseJson, HttpStatus.BAD_REQUEST);
    }
}