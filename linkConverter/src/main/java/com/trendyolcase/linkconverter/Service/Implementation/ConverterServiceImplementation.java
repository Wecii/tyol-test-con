package com.trendyolcase.linkconverter.Service.Implementation;

import com.trendyolcase.linkconverter.Constant.Constants;
import com.trendyolcase.linkconverter.Controller.ConverterController;
import com.trendyolcase.linkconverter.Entity.Traffic;
import com.trendyolcase.linkconverter.Exception.InputNotValidException;
import com.trendyolcase.linkconverter.Exception.InputParsingException;
import com.trendyolcase.linkconverter.Exception.InputTypeMatchingException;
import com.trendyolcase.linkconverter.Object.Deeplink;
import com.trendyolcase.linkconverter.Object.ResponseJson;
import com.trendyolcase.linkconverter.Object.ServiceInput;
import com.trendyolcase.linkconverter.Object.WebURL;
import com.trendyolcase.linkconverter.Repository.TrafficRepository;
import com.trendyolcase.linkconverter.Service.ConverterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Service
public class ConverterServiceImplementation implements ConverterService {

    private static final Logger logger = LoggerFactory.getLogger(ConverterServiceImplementation.class);

    private final TrafficRepository trafficRepository;
    public ConverterServiceImplementation(TrafficRepository trafficRepository) {
        this.trafficRepository = trafficRepository;
    }

    @Override
    public ResponseJson convertFromWeb(ServiceInput input) throws UnsupportedEncodingException {

        if (!input.getSourceType().equals(Constants.webURLSourceType)) {
            throw new InputTypeMatchingException("Submitted request endpoint is for webURL but input is for deeplink");
        }

        String existenceControl = checkExistence(input);
        if(existenceControl != null) return new ResponseJson(1,"Success",existenceControl);

        isValidWebURL(input.getInput());

        WebURL webURL = new WebURL();
        webURL.parse(input.getInput());

        Deeplink deeplink = convertToDeeplink(webURL);

        logger.info("From WebURL to Deeplink conversion succeed.");

        Traffic traffic = new Traffic(webURL.getRaw(), deeplink.getRaw(), "web");
        trafficRepository.save(traffic);

        return new ResponseJson(1, "Success", deeplink.getRaw());
    }

    @Override
    public ResponseJson convertFromDeeplink(ServiceInput input) throws UnsupportedEncodingException {

        if (!input.getSourceType().equals(Constants.deeplinkSourceType)) {
            throw new InputTypeMatchingException("Submitted request endpoint is for Deeplink but input is for webURL");
        }

        String existenceControl = checkExistence(input);
        if(existenceControl != null) return new ResponseJson(1,"Success",existenceControl);

        isValidDeeplink(input.getInput());

        Deeplink deeplink = new Deeplink();
        deeplink.parse(input.getInput());

        WebURL webURL = convertToWebURL(deeplink);

        logger.info("From Deeplink to WebURL conversion succeed.");

        Traffic traffic = new Traffic(deeplink.getRaw(), webURL.getRaw(), "deeplink");
        trafficRepository.save(traffic);

        return new ResponseJson(1, "Success", webURL.getRaw());
    }


    /** Checking already inserted before */
    private String checkExistence(ServiceInput input){

        Traffic trafficByFromRaw = trafficRepository.findByFromRaw(input.getInput());
        if(trafficByFromRaw != null) return trafficByFromRaw.getTargetRaw();

        /**
         * FindByTargetRaw ignored because of "/brand/name-p-" issue
         */

        return null;

    }

    /** Validations */
    private void isValidWebURL(String webURL) {
        if (webURL == null || webURL.isBlank() || webURL.isEmpty()){
            throw new InputNotValidException("WebURL can not be null or blank or empty");
        }
        if (!webURL.contains("https://www.trendyol.com")){
            throw new InputNotValidException("WebURL must contains https://www.trendyol.com");
        }

        webURL = webURL.replace("https://www.trendyol.com/", "");

        if (webURL.contains("-p-")) {
            isValidProductWebURL(webURL);
        } else if (webURL.contains("sr?q=")) {
            isValidSearchWebURL(webURL);
        }
    }

    private void isValidProductWebURL(String webURL) {

        String[] parts = webURL.split("/");

        if(parts.length < 2) throw new InputNotValidException("\"BrandOrCategory\" or \"ProductNameAndContentId\" is missing or corrupted.");

        String brandOrCategory = parts[0];
        if (brandOrCategory.length() < 1 || brandOrCategory.isBlank()){
            throw new InputNotValidException("\"BrandOrCategory\" field can not be empty or blank or null");
        }

        String productNameAndContentId;
        try {
            productNameAndContentId = parts[1].replaceAll("//?", "?").split("\\?")[0];
        }catch (Exception ex){
            throw new InputParsingException("\"ProductNameAndContentId\" field missing or corrupted.");
        }

        String productName = productNameAndContentId.split("-p-")[0];
        if (productName.length() < 1 || productName.isBlank()){
            throw new InputNotValidException("\"ProductName\" field can not be empty or blank or null");
        }

        String contentID = productNameAndContentId.split("-p-")[1];
        if(contentID.length() < 1 || contentID.isBlank()){
            throw new InputNotValidException("\"contentID\" field can not be empty or blank or null");
        }
    }

    private void isValidSearchWebURL(String webURL) {
        String searchQuery = webURL.replace("sr?q=", "");
        if(searchQuery.length() < 1 || searchQuery.isBlank()){
            throw new InputNotValidException("\"searchQuery\" field can not be empty or blank or null");
        }
    }

    private void isValidDeeplink(String deeplink) {
        if (deeplink == null || deeplink.isBlank() || deeplink.isEmpty()){
            throw new InputNotValidException("Deeplink can not be null or blank or empty");
        }
        if (!deeplink.contains("ty://?")){
            throw new InputNotValidException("Deeplink must contains ty://?");
        }
        if(!deeplink.contains("Page=")){
            throw new InputNotValidException("Deeplink must contains Page parameter");
        }
        if(deeplink.contains("Page=Product") && !deeplink.contains("ContentId")){
            throw new InputNotValidException("Deeplink must contains ContentId parameter if already has Pagetype tagged as Product");
        }
    }

    /** Conversions */
    private Deeplink convertToDeeplink(WebURL webURL) throws UnsupportedEncodingException {

        StringBuilder builder = new StringBuilder();
        builder.append("ty://?Page=");
        builder.append(webURL.getAddressType());

        if (webURL.getAddressType().equals("Product")) {
            builder.append(prepareProductDeeplink(webURL).toString());
        }

        if (webURL.getAddressType().equals("Search")) {
            builder.append(prepareSearchDeeplink(webURL).toString());
        }

        Deeplink deeplink = new Deeplink();
        deeplink.parse(builder.toString());
        return deeplink;

    }

    private static StringBuilder prepareSearchDeeplink(WebURL webURL) {
        StringBuilder builder = new StringBuilder();

        if (webURL.getSearchQuery() != null && !webURL.getSearchQuery().isEmpty()) {
            builder.append("&Query=");
            builder.append(webURL.getSearchQuery());
        }

        return builder;
    }

    private static StringBuilder prepareProductDeeplink(WebURL webURL) {

        StringBuilder builder = new StringBuilder();

        if (webURL.getContentId() != null && !webURL.getContentId().isEmpty()) {
            builder.append("&ContentId=");
            builder.append(webURL.getContentId());
        }

        if (webURL.getBoutiqueId() != null && !webURL.getBoutiqueId().isEmpty()) {
            builder.append("&CampaignId=");
            builder.append(webURL.getBoutiqueId());
        }

        if (webURL.getMerchantId() != null && !webURL.getMerchantId().isEmpty()) {
            builder.append("&MerchantId=");
            builder.append(webURL.getMerchantId());
        }

        return builder;

    }

    private WebURL convertToWebURL(Deeplink deeplink) throws UnsupportedEncodingException {

        StringBuilder builder = new StringBuilder();
        builder.append("https://www.trendyol.com/");
        if (deeplink.getLinkType().equals("Product")) {
            builder.append(prepareProductWebURL(deeplink).toString());
        }

        if (deeplink.getLinkType().equals("Search")) {
            builder.append(prepareSearchWebURL(deeplink).toString());
        }

        WebURL webURL = new WebURL();
        webURL.parse(builder.toString());
        return webURL;

    }

    private static StringBuilder prepareProductWebURL(Deeplink deeplink) {

        StringBuilder builder = new StringBuilder();
        builder.append("brand/name-p-");

        builder.append(deeplink.getContentId());
        Boolean isContainsMerchantId = deeplink.getMerchantId() != null && !deeplink.getMerchantId().isEmpty();
        Boolean isContainsBoutiqueId = deeplink.getCampaignId() != null && !deeplink.getCampaignId().isEmpty();

        if (isContainsBoutiqueId || isContainsMerchantId) builder.append("?");

        if (isContainsBoutiqueId) {
            builder.append("boutiqueId=");
            builder.append(deeplink.getCampaignId());
        }

        if (isContainsBoutiqueId && isContainsMerchantId) {
            builder.append("&");
        }

        if (isContainsMerchantId) {
            builder.append("merchantId=");
            builder.append(deeplink.getMerchantId());
        }

        return builder;

    }

    private static StringBuilder prepareSearchWebURL(Deeplink deeplink) {
        StringBuilder builder = new StringBuilder();
        builder.append("sr?q=");

        if (deeplink.getSearchQuery() != null && !deeplink.getSearchQuery().isEmpty()) {
            builder.append(deeplink.getSearchQuery());
        }

        return builder;
    }
}
