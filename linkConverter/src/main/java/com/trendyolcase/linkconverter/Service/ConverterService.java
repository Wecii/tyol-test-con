package com.trendyolcase.linkconverter.Service;

import com.trendyolcase.linkconverter.Object.ResponseJson;
import com.trendyolcase.linkconverter.Object.ServiceInput;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

public interface ConverterService {
    ResponseJson convertFromWeb(ServiceInput input) throws MalformedURLException, UnsupportedEncodingException;
    ResponseJson convertFromDeeplink(ServiceInput input) throws UnsupportedEncodingException;
}
