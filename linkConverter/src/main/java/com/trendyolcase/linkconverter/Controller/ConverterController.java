package com.trendyolcase.linkconverter.Controller;

import com.trendyolcase.linkconverter.Object.ResponseJson;
import com.trendyolcase.linkconverter.Object.ServiceInput;
import com.trendyolcase.linkconverter.Service.ConverterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

@RestController
public class ConverterController {

    private static final Logger logger = LoggerFactory.getLogger(ConverterController.class);

    private final ConverterService converterService;
    public ConverterController(ConverterService converterService) {
        this.converterService = converterService;
    }

    /**
     * This endpoint is used to convert web addresses to deeplink addresses.
     */
    @RequestMapping(value = {"/convert/web"}, produces = "application/json", method = RequestMethod.POST)
    public ResponseJson convertFromWebAddress(@RequestBody ServiceInput serviceInput) throws MalformedURLException, UnsupportedEncodingException {
        logger.info("Endpoint : WebURL to Deeplink, input : "+serviceInput.getInput());
        return converterService.convertFromWeb(serviceInput);
    }

    /**
     * This endpoint is used to convert deeplink addresses to web addresses.
     */
    @RequestMapping(value = {"/convert/deeplink"}, produces = "application/json", method = RequestMethod.POST)
    public ResponseJson convertFromDeeplinkAddress(@RequestBody ServiceInput serviceInput) throws UnsupportedEncodingException {
        logger.info("Endpoint : Deeplink to WebURL, input : "+serviceInput.getInput());
        return converterService.convertFromDeeplink(serviceInput);
    }

}
