package com.trendyolcase.linkconverter.Object;

import com.trendyolcase.linkconverter.Constant.Constants;
import com.trendyolcase.linkconverter.Exception.InputNotValidException;
import com.trendyolcase.linkconverter.Exception.InputParsingException;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class Deeplink implements Serializable {

    private String linkType;
    private String contentId;
    private String campaignId;
    private String merchantId;
    private String searchQuery;
    private String raw;

    private void parseKeyValuePairArray(String[] keyValuePairArray) throws UnsupportedEncodingException {
        String value = keyValuePairArray[1];
        String decodedValue = URLDecoder.decode(value,Constants.utf8Encoding);
        if(decodedValue.equals(value)) value = URLEncoder.encode(value,Constants.utf8Encoding);

        if(keyValuePairArray[0].trim().equals("Page")){
            this.linkType = value;
            return;
        }

        boolean isQuery = keyValuePairArray[0].trim().equals("Query");
        boolean isSearch = this.linkType.equals("Search");
        boolean isContentId = keyValuePairArray[0].trim().equals("ContentId");
        boolean isCampaignId = keyValuePairArray[0].trim().equals("CampaignId");
        boolean isMerchantId = keyValuePairArray[0].trim().equals("MerchantId");

        if(isQuery && isSearch) this.searchQuery = value;
        if(isContentId) this.contentId = value;
        if(isCampaignId) this.campaignId = value;
        if(isMerchantId) this.merchantId = value;
    }

    public void parse(String deeplink) throws UnsupportedEncodingException {
        this.raw = deeplink;
        deeplink = deeplink.replace(Constants.deeplinkDefaultScheme,"");

        for(String keyAndValue : deeplink.split(Constants.ampersand)){
            String[] keyValuePairArray = keyAndValue.split(Constants.equal);

            if(keyValuePairArray.length != 2) throw new InputNotValidException("Query Parameter Separation Error.");

            parseKeyValuePairArray(keyValuePairArray);
        }

        if(!this.linkType.matches("Search|Product") && !this.linkType.isEmpty()){
            this.linkType = "Home";
        }

        if(this.linkType.isEmpty() || this.linkType.isBlank()){
            throw new InputParsingException("Page parameter could not parsed.");
        }
    }


    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }
}
