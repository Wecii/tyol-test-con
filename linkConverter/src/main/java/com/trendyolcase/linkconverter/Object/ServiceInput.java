package com.trendyolcase.linkconverter.Object;

import com.trendyolcase.linkconverter.Exception.InputNotValidException;

import java.io.Serializable;

public class ServiceInput implements Serializable {

    public ServiceInput() {
    }

    private String input;

    public ServiceInput(String input) {
        this.input = input.trim();
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input.trim();
    }

    public String getSourceType(){
        if(this.input.contains("https://www.trendyol.com")) return "web";
        if(this.input.contains("ty://?Page")) return "deeplink";

        throw new InputNotValidException("address should be started with ty://?Page or https://www.trendyol.com");
    }

    @Override
    public String toString() {
        return "ServiceInput{" +
                "input='" + input + '\'' +
                '}';
    }
}
