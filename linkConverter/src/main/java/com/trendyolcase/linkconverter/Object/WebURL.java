package com.trendyolcase.linkconverter.Object;

import com.trendyolcase.linkconverter.Constant.Constants;
import com.trendyolcase.linkconverter.Exception.InputNotValidException;
import com.trendyolcase.linkconverter.Exception.InputParsingException;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class WebURL implements Serializable {

    private String addressType;
    private String brandOrCategory;
    private String productName;
    private String contentId;
    private String boutiqueId;
    private String merchantId;
    private String searchQuery;
    private String raw;

    public WebURL() {
    }

    public void parse(String url) throws UnsupportedEncodingException {

        this.raw = url;

        url = url.replace("https://www.trendyol.com/", "");

        if (url.contains("-p-")) {
            parseProductAddress(url);
        } else if (url.contains("sr?q=")) {
            parseSearchAddress(url);
        }else{
            this.addressType = "Home";
        }

    }

    private void parseBrandOrCategory(String brandOrCategory){
        if (brandOrCategory.length() < 1 || brandOrCategory.isBlank()) throw new InputParsingException("Brand or Category could not parsed.");
        this.brandOrCategory = brandOrCategory;
    }

    private void parseProductNameAndContentId(String productNameAndContentId){
        productNameAndContentId = productNameAndContentId.replaceAll("//?", "?").split("\\?")[0];

        String productName = productNameAndContentId.split(Constants.webURLProductBrace)[0];
        if (productName.length() < 1 || productName.isBlank()) throw new InputParsingException("ProductName could not parsed.");
        this.productName = productName;

        String contentID = productNameAndContentId.split(Constants.webURLProductBrace)[1];
        if (contentID.length() < 1 || contentID.isBlank())  throw new InputParsingException("ContentId could not parsed.");
        this.contentId = contentID;
    }

    private void parseQueryParameters(String queryParameters) throws UnsupportedEncodingException {
        String boutiqueId = "";
        String merchantId = "";
        for (String keyAndValue : queryParameters.split(Constants.ampersand)) {

            String[] keyValuePairArray = keyAndValue.split("=");
            if (keyValuePairArray.length != 2) throw new InputNotValidException("Query Parameter Seperation Error.");

            String value = keyValuePairArray[1];
            String decodedValue = URLDecoder.decode(value, Constants.utf8Encoding);
            if (decodedValue.equals(value)) value = URLEncoder.encode(value, Constants.utf8Encoding);

            boolean isBoutiqueId = keyValuePairArray[0].trim().equals("boutiqueId");
            if (isBoutiqueId) {
                boutiqueId = value;
                continue;
            }

            boolean isMerchantId = keyValuePairArray[0].trim().equals("merchantId");
            if (isMerchantId) merchantId = value;
        }
        this.boutiqueId = boutiqueId;
        this.merchantId = merchantId;
    }

    private void parseProductAddress(String url) throws UnsupportedEncodingException {
        String[] parts = url.split(Constants.slash);
        this.addressType = "Product";

        parseBrandOrCategory(parts[0]);
        parseProductNameAndContentId(parts[1]);

        if (!parts[1].contains("?")) return;
        parseQueryParameters(parts[1].split("\\?")[1].replace("?", ""));
    }

    private void parseSearchAddress(String url) throws UnsupportedEncodingException {

        String searchQuery = url.replace(Constants.webURLSearchQueryDefaultText, "");

        if (searchQuery.length() < 1 || searchQuery.isBlank()) throw new InputParsingException("searchQuery could not parsed.");

        String decodedSearchQuery = URLDecoder.decode(searchQuery, Constants.utf8Encoding);
        if (decodedSearchQuery.equals(searchQuery)) searchQuery = URLEncoder.encode(searchQuery, Constants.utf8Encoding);

        this.searchQuery = searchQuery;
        this.addressType = "Search";

    }


    /**
     * Getters and Setters
     */

    public String getAddressType() {
        return addressType;
    }

    public String getBrandOrCategory() {
        return brandOrCategory;
    }

    public String getProductName() {
        return productName;
    }

    public String getContentId() {
        return contentId;
    }

    public String getBoutiqueId() {
        return boutiqueId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public String getRaw() {
        return raw;
    }
}
