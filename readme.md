# Trendyol Link Converter Backend Applicant Test

There is an implementation of a web service that helps you to convert [Trendyol](https://www.trendyol.com/) links between mobile and web applications.

## <ins>Services<ins>


### Web URL Service
Provides conversion from web address to deeplink

| Method | Path              | Body
|--------|-------------------|-----------------------------------------------|
| POST   | /convert/web      | {"input":"URL"}                               |


### Deeplink Service
Provides conversion from deeplink to web address

| Method | Path              | Body
|--------|-------------------|-----------------------------------------------|
| POST   | /convert/deeplink | {"input":"Deeplink"}                          |


## <ins>Database<ins>

MySQL is used for persistent data storage for services in this application.

A valid MySQL Connection URL must be specified in properties file.

For testing : `resources/application-test.properties`

For Production : `resources/application.properties`




## <ins>Installation<ins>

#### With Docker and Docker Compose

- `docker-compose up --build`

#### Without Docker

- Install Java (15)

- `./gradlew clean build --info`

- `java -jar '-Dspring.profiles.active=prod' build/libs/linkconverter-0.0.1-SNAPSHOT.jar`


## <ins>Local Development<ins>

- For local development, you need to create a file in directory resources named as `application-dev.properties`
- You can use the same scheme of `application.properties` file after changing values of properties for development
- For local development builds, you can use `'-Dspring.profiles.active=dev'` argument